module goMaker

go 1.14

require github.com/urfave/cli v1.22.5

require (
	gitee.com/RickieL/comm v1.0.3
	github.com/cpuguy83/go-md2man/v2 v2.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
)
